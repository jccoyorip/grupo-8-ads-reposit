# UNIVERSIDAD CÉSAR VALLEJO

FACULTAD DE INGENIERIA
Escuela de Ingeniería de Sistemas

## Desarrollo de una aplicación web para el control del progreso de los  clientes  del  gimnasio “TOTAL FITNESS”  Lima - Lima - 2022  

TOTAL FITNESS


### Profesor RICHARD LEONARDO BERROCAL NAVARRO

#### EQUIPO NRO.8

1. Rodríguez Rojas, Kevin
2. Sandi Chiquillan, Ovaldi Jesus
3. Diaz Aguilar, Gian Carlos
4. Ccoyori Pérez, José Antonio
5. Salazar Rojas, Sharon Denil
6. Orosco Machacuay , Melissa Anais



 INTRODUCCIÓN 

A finales del 2019 , apareció una nueva enfermedad que se extendió rápidamente por todo el mundo por lo que fue declarado como pandemia , esto llevó a que los diferentes gobiernos del mundo aislen a su  población para  poder  controlar su propagación , si bien al inicio fue de mucha ayuda las personas empezaron a sufrir las consecuencias  del encierro  se presentaron situaciones que afectaron su salud física y mental en su mayoría de casos , por la tristeza , soledad , ansiedad , depresión y el miedo que generaba la incertidumbre de la situación . Esto desencadenó como consecuencia  un descuido en la alimentación de las personas  causando un alza en los casos de sobrepeso en gran cantidad de países en el mundo. Nuestro país no fue ajeno a esta situación, por lo cual los habitantes  optaron en su mayoría por  inscribirse en gimnasios con el fin de mejorar su salud, sin embargo  muchos de estos seguían utilizando control básico del progreso de los clientes por medio de registros convencionales en otras palabras un control mediante apuntes,  en el presente proyecto implementaremos una aplicación web para el control del progreso de los  clientes  del  gimnasio TOTAL FITNESS.A, que en su mayoría están en el rango de edad 18 - 50 años, el objetivo del desarrollo de la aplicación web  es brindar una información detallada del progreso (Peso, Masa Muscular), de los clientes, para que lleven un control más  ordenado de sus rutinas y así poder motivarse a seguir asistiendo al gimnasio para  seguir cuidando su salud .


1. ESTUDIO DE FACTIBILIDAD
 
 
 En el siguiente proyecto se realizará  un estudio de factibilidad donde determinaremos  si el proyecto es viable, para ello mediante criterios determinamos la factibilidad del proyecto.

 | CRITERIOS | SE REQUIERE |
| --- | ---: |
| Fundamentación Teórica |Acceso a la parte teórica para la fundamentación del proyecto, programas complementarios. | 
| Posibilidad de acceso a los recursos requeridos por el proyecto | Disponibilidad de recursos tecnológicos, que necesita el proyecto.| 
| Accesos a las fuentes de información. | Acceso directo a la informacion sea por web y al negocio 


1.1. Factibilidad operativa y técnica: La visión del sistema

FACTIBILIDAD OPERATIVA 
Existen las condiciones adecuadas para el desarrollo y su posterior comprobación. Este proyecto puede tener un buen impacto en los clientes del gimnasio TOTAL FITNESS S.A , dado que a las personas que va dirigido les gusta mantener un buen acondicionamiento físico pero los primeros meses son difíciles y tienden a desistir de entrenar, con el sistema se busca que esto no pase y puedan observar una mejora en su progreso personal y salud .

FACTIBILIDAD TÉCNICA

 | Componentes | Especificaciones mínimas |
| ---: | ---:|
|Procesador|Cpu 1.8 GHz|
|Memoria|2 Gb|
|Disco duro|500 Gb|
|Dispositivos de entrada y salida |Estandar|

2. MODELO DEL NEGOCIO


   Un diagrama de proceso empresarial es un diagrama que describe un flujo dirigido de actividades especificadas utilizando un subconjunto de notación para la creación de modelos de proceso de negocio. Un proceso simple representa los procesos internos que ocurren en una unidad organizativa o empresarial.

   ![Insertando imagen](1.jpg)
   Imagen No 1. - Diagrama Eriksson - Penker
   

 2.1. Modelo de Caso de Uso del Negocio

El cliente solicita una apertura de membresía al recepcionista del Gimnasio, el recepcionista genera formulario para apertura de membresía registra el formulario al sistema y de acuerdo a las respuestas del cliente, se toma la decisión si se apertura la membresía, si se apertura se asigna un instructor y se entregará sus credenciales,  sus que esas serán registradas y para su respectivo control.


| Lista de actores del negocio |  |
| ---: | ---:|
|Nombre|Descripción|
|Cliente|Persona que requiere el servicio, de rutina.|
|Recepcionista|Persona responsable en el registro del cliente y la asignación del entrenador, el usuario registra su avance de rutina|
|Entrenador|Persona encargada en el siguiento del cliente para el desarrollo de su rutina|

![Insertando imagen](2.jpg)
Imagen No 2. - Actores de Negocio 

2.1.2. Lista de Casos de Uso del Negocio

| Lista casos de Uso del Negocio |  |
| ---: | ---:|
|Nombre|Descripción|
|Llenar formulario|El cliente solicitará en percepción su respectivo formulario para la apertura de membresía. |
|Generar formulario de apertura.|El Recepcionista genera Formulario, para que el cliente pueda completarlos.|
|Asignar al Entrenador|Una vez que el formulario está completado, se realizará una verificación de los datos ingresados, el instructor verificará las preguntas relacionadas a su ámbito|

Tabla No. 2 – Lista de Casos de Uso del Negocio

![Insertando imagen](3.jpg)
Imagen No.3 - Casos de uso de Negocio

2.1.3. Diagrama de Casos de Uso del Negocio
![Insertando imagen](4.jpg)
Imagen  No. 4. Diagrama de Casos de Uso del Negocio

2.1.4. Especificaciones de Casos de uso del Negocio

En la especificación de los casos de uso de negocio encontraremos paso a paso el funcionamiento de nuestro proceso.

| Nombre | Llenar formulario |
| ---: | ---:|
|Descripción|El cliente solicitará en percepción su respectivo formulario para la apertura de membresía.  |
|Actores de negocio|Cliente, Recepcionista. |
|Entradas|Información, formularios.|
|Entregables|Membresía|
|Mejoras |Registro, vía web para agilizar y que el cliente pueda completarlo sin dificultad|

 Tabla No. 3.1. – ECUN  1

| Nombre | Generar formulario de apertura.|
| ---: | ---:|
|Descripción|El Recepcionista genera Formulario, para que el cliente pueda completarlos.  |
|Actores de negocio|Recepcionista.  |
|Entradas|Información del cliente.|
|Entregables|Formulario de apertura.|
|Mejoras |El formulario sea digital, y sea enviado al correo del cliente.|

Tabla No. 3.2. – ECUN  2

| Nombre | Asignar al Entrenador.|
| ---: | ---:|
|Descripción|Una vez que el formulario está completado, se realizará una verificación de los datos ingresados, el instructor verificará las preguntas relacionadas a su ámbito. |
|Actores de negocio|Recepcionista, entrenador.  |
|Entradas|Respuestas de cliente, mediante el formulario.|
|Entregables|Registro por sistema, datos del instructor.|
|Mejoras |En el sistema web se asignará al instructor, el cliente visualizará los datos del entrenador.|

Tabla No. 3.3. – ECUN  3

| Nombre | Entregar credencial al cliente.|
| ---: | ---:|
|Descripción|Una vez que el formulario fue aceptado, se hace entrega de la credencial.  |
|Actores de negocio|Recepcionista.|
|Entradas|Formulario aceptado. |
|Entregables|Credencial.|
|Mejoras |Con la credencial podrá registrar sus actividades, realizar un control más responsable de los ejercicios realizados. |

Tabla No. 3.4. – ECUN  4

2.2. Modelo de Análisis del Negocio

A diferencia del modelo de caso de negocio en el modelo de análisis pasamos a una vista   interna   de sus procesos, pudiendo analizar los trabajadores del  Gimnasio  “Total fitness” y además identificamos las entidades que se manejan en nuestro proceso.

2.2.1. Lista de Trabajadores de Negocio

![Insertando imagen](5.jpg)

Tabla No. 4. Trabajadores del Negocio

2.2.2. Lista de Entidades de Negocio

![Insertando imagen](6.jpg)
Tabla No. 5. Entidades del Negocio
